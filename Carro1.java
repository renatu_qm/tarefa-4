package carro;
public class Carro1 {

    public static final String AZUL = "Azul";
    public static String COMUM = "Comum";
    public static String CHASSI = "9BWHE21JX2406060";
    public static String DATA = "1997";

        private Integer numeroPortas;
        private String anoFabricacao;
        private String numeroChassi;
        private String combustivel;
        private String cor;

    public Carro1(Integer numeroPortas) {
        this.numeroPortas = numeroPortas;
    }


    public static String getCOMUM() {
        return COMUM;
    }

    public static String getCHASSI() {
        return CHASSI;
    }

    public String getCombustivel() {
        return combustivel;
      }

      public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }
    public String getNumeroChassi() {
        return numeroChassi;
    }

    public void setNumeroChassi(String numeroChassi) {
        this.numeroChassi = numeroChassi;
    }


    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public Integer getNumeroPortas() {
        return numeroPortas;
    }


    public String getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao(String anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    public void imprimeValores(){

        System.out.println("Número de Portas: " + getNumeroPortas());
        System.out.println("Ano de Fabricação: " + getAnoFabricacao());
        System.out.println("Número de Chassi: " + getNumeroChassi());
        System.out.println("Combustivel: " + getCombustivel());
        System.out.println("Cor: " + getCor());
    }
}





