import carro.Carro1;

public class Main1 {

    public static void main(String... args) {
        Carro1 carro1 = new Carro1(3);
        carro1.setCor(Carro1.AZUL);
        carro1.setCombustivel(Carro1.COMUM);
        carro1.setNumeroChassi(Carro1.CHASSI);
        carro1.setAnoFabricacao(Carro1.DATA);

        carro1.imprimeValores();
    }
}
